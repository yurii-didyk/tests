﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Models;

namespace ProjectStructureWebAPI.Context
{
    public class DataContext: DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Models.Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Birthday = new DateTime(1995, 4,5),
                    RegisteredAt = new DateTime(2019,6,7),
                    Email = "user1@gmail.com",
                    FirstName = "Andrii",
                    LastName = "Johnson",
                    TeamId = 3
                },
                new User
                {
                    Id = 2,
                    Birthday = new DateTime(1993, 10,5),
                    RegisteredAt = new DateTime(2019,5,3),
                    Email = "user2@gmail.com",
                    FirstName = "Danyka",
                    LastName = "Gusikowski",
                    TeamId = 1
                },
                new User
                {
                    Id = 3,
                    Birthday = new DateTime(1991, 10,2),
                    RegisteredAt = new DateTime(2019,4,23),
                    Email = "user3@gmail.com",
                    FirstName = "Anibal",
                    LastName = "Hintz"
                },
                new User
                {
                    Id = 4,
                    Birthday = new DateTime(1998, 8,8),
                    RegisteredAt = new DateTime(2019,6,1),
                    Email = "user4@gmail.com",
                    FirstName = "Geovanny",
                    LastName = "Torphy"
                },

            };

            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    Name = "Sunt",
                    CreatedAt = new DateTime(2019,6,8)
                },
                new Team
                {
                     Id = 2,
                    Name = "Reprehenderit",
                    CreatedAt = new DateTime(2019,6,1)
                    
                },
                new Team
                {
                     Id = 3,
                    Name = "Repudiandae",
                    CreatedAt = new DateTime(2019,5,22)
                    
                },
                new Team
                {
                     Id = 4,
                    Name = "Labore",
                    CreatedAt = new DateTime(2019,4,18)
                }

            };
           

            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1, Name = "Rerum voluptatem beatae nesciunt consectetur.",
                    Description = "Ut quisquam molestias ut.\nQuia laudantium est quo asperiores veritatis porro ut quod doloribus.\nReiciendis odit ut hic libero.\nFuga debitis veniam ut.",
                    CreatedAt = DateTime.Now, Deadline = new DateTime(2019, 10, 22),
                    AuthorId = 1, TeamId = 3
                },
                new Project
                {
                    Id = 2, Name = "Atque quia et optio aut.",
                    Description = "Non non aperiam aspernatur et est mollitia enim quia nihil.",
                    CreatedAt = new DateTime(2019, 5, 12), Deadline = new DateTime(2019, 10, 24),
                    AuthorId = 2, TeamId = 1
                },
                new Project
                {
                    Id = 3, Name = "Vel aperiam qui vel animi.",
                    Description = "Cupiditate veritatis ad rerum veritatis molestiae.\nDolorem soluta adipisci fuga iste aut est eveniet et.",
                    CreatedAt = new DateTime(2018, 12,12), Deadline = new DateTime(2019, 11, 2),
                    AuthorId = 4, TeamId = 2
                },
                new Project
                {
                    Id = 4, Name = "Eius fugiat amet molestias eos.",
                    Description = "Debitis et ipsum esse perferendis voluptatem amet eos.",
                    CreatedAt = DateTime.Now, Deadline = new DateTime(2019, 12, 29),
                    AuthorId = 3, TeamId = 4
                }
            };

            var tasks = new List<Models.Task>
            {
                new Models.Task
                {
                    Id = 1, Name="Eaque corporis illum ut.",
                    Description = "Inventore quae alias magnam sed qui sint velit est.\nSuscipit dolor ut omnis voluptate.\nExercitationem qui et temporibus dignissimos autem.",
                    CreatedAt = new DateTime(2019,4,7), FinishedAt = new DateTime(2019,7,2),
                    PerformerId = 3, ProjectId = 2, State = TaskState.Finished
                },
                new Models.Task
                {
                    Id = 2, Name="Ipsam ea voluptatem optio sed doloremque.",
                    Description = "Aut eligendi aut quaerat explicabo impedit.\nPraesentium est inventore nostrum.\nVel quasi deserunt id sapiente iste voluptas est.",
                    CreatedAt = new DateTime(2019,4,3), FinishedAt = new DateTime(2019,8,2),
                    PerformerId = 4, ProjectId = 1, State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 3, Name="Debitis aliquid dicta maiores sed molestiae",
                    Description = "Et et ea voluptas laboriosam officiis ut.\nQuia deleniti itaque.\nEst molestiae magnam et nesciunt.",
                    CreatedAt = new DateTime(2019,3,9), FinishedAt = new DateTime(2019,10,10),
                    PerformerId = 2, ProjectId = 2, State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 4, Name="Consequatur rerum rem eos ea.",
                    Description = "Nobis eius eos inventore nostrum autem eos.\nPariatur iusto saepe et quas ratione consequuntur aut.",
                    CreatedAt = new DateTime(2019,4,12), FinishedAt = new DateTime(2019,8,9),
                    PerformerId = 3, ProjectId = 4, State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 5, Name="Voluptate distinctio id saepe reprehenderit in sint quidem.",
                    Description = "Corporis necessitatibus mollitia deleniti harum porro molestiae.\nNihil est unde quidem est molestiae nisi sint.",
                    CreatedAt = new DateTime(2019,6,9), FinishedAt = new DateTime(2019,11,10),
                    PerformerId = 1, ProjectId = 1, State = TaskState.Canceled
                },
                new Models.Task
                {
                    Id = 6, Name="Harum quaerat impedit sed iure quia labore error rerum.",
                    Description = "Voluptate sunt perferendis voluptates vitae non.\nFacilis aliquam quidem aut consequatur magni facilis eaque.",
                    CreatedAt = new DateTime(2019,4,16), FinishedAt = new DateTime(2019,10,9),
                    PerformerId = 4, ProjectId = 2, State = TaskState.Created
                }

            };

            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Models.Task>().HasData(tasks);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);

            base.OnModelCreating(modelBuilder);
        }
    }
}
