﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using AutoMapper;
using ProjectStructureWebAPI.Context;
using ProjectStructureWebAPI.Repositories;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Services
{
    public class TaskService
    {
        private readonly IRepository<Models.Task> _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TaskService(IRepository<Models.Task> context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async Task Add(TaskDTO taskDto)
        {
            await _queueService.Post("Task creation was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            await _context.Create(task);
        }
        public async Task Delete(int id)
        {
            await _queueService.Post("Task deletion was triggered");
            await _context.Delete(id);
        }
        public async Task Update(int id, TaskDTO taskDto)
        {
            await _queueService.Post("Task updating was triggered");
            var task = _mapper.Map<Models.Task>(taskDto);
            await _context.Update(id, task);
        }
        public async Task<TaskDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single task was triggered");
            var task = await _context.GetSingleItem(id);
            return _mapper.Map<TaskDTO>(task);
        }
        public async Task<IEnumerable<TaskDTO>> Get()
        {
            await _queueService.Post("Loading all tasks was triggered");
            var tasks = await _context.GetItems();
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }

    }
}
