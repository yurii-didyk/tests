﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using ProjectStructureWebAPI.Repositories;
using Microsoft.EntityFrameworkCore;
using AutoMapper;

namespace ProjectStructureWebAPI.Services
{
    public class ProjectService
    {
        private readonly IRepository<Project> _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public ProjectService(IRepository<Project> context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(ProjectDTO projectDto)
        {
            await _queueService.Post("Project creation was triggered");
            var project = _mapper.Map<Project>(projectDto);
            await _context.Create(project);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("Project deletion was triggered");
            await _context.Delete(id);
        }
        public async System.Threading.Tasks.Task Update(int id, ProjectDTO projectDto)
        {
            await _queueService.Post("Project updating was triggered");
            var project = _mapper.Map<Project>(projectDto);
            await _context.Update(id, project);
        }
        public async Task<ProjectDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single project was triggered");
            var project = await _context.GetSingleItem(id);
            return _mapper.Map<ProjectDTO>(project);
        }
        public async Task<IEnumerable<ProjectDTO>> Get()
        {
            await _queueService.Post("Loading all projects was triggered");
            var projects = await _context.GetItems();
            return _mapper.Map<IEnumerable<ProjectDTO>>(projects);
        }
    }
}
