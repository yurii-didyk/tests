﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Context;
using ProjectStructureWebAPI.Repositories;

namespace ProjectStructureWebAPI.Services
{
    public class UserService
    {
        private readonly IRepository<User> _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;


        public UserService(IRepository<User> context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(UserDTO teamDto)
        {
            await _queueService.Post("User creation was triggered");
            var user = _mapper.Map<User>(teamDto);
            await _context.Create(user);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("User deletion was triggered");
            await _context.Delete(id);
        }
        public async System.Threading.Tasks.Task Update(int id, UserDTO teamDto)
        {
            await _queueService.Post("User updating was triggered");
            var user = _mapper.Map<User>(teamDto);
            await _context.Update(id, user);
        }
        public async Task<UserDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single user was triggered");
            var user = await _context.GetSingleItem(id);
            return _mapper.Map<UserDTO>(user);
        }
        public async Task<IEnumerable<UserDTO>> Get()
        {
            await _queueService.Post("Loading all users was triggered");
            var users = await _context.GetItems();
            return _mapper.Map<IEnumerable<UserDTO>>(users);
        }
    }
}