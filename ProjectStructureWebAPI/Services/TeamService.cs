﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.DTOs;
using ProjectStructureWebAPI.Models;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Context;
using ProjectStructureWebAPI.Repositories;

namespace ProjectStructureWebAPI.Services
{
    public class TeamService
    {
        private readonly IRepository<Team> _context;
        private readonly IMapper _mapper;
        private readonly QueueService _queueService;

        public TeamService(IRepository<Team> context,
            IMapper mapper, QueueService queueService)
        {
            _context = context;
            _mapper = mapper;
            _queueService = queueService;
        }

        public async System.Threading.Tasks.Task Add(TeamDTO teamDto)
        {
            await _queueService.Post("Team creating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            await _context.Create(team);
        }
        public async System.Threading.Tasks.Task Delete(int id)
        {
            await _queueService.Post("Team deletion was triggered");
            await _context.Delete(id);
        }
        public async System.Threading.Tasks.Task Update(int id, TeamDTO teamDto)
        {
            await _queueService.Post("Team updating was triggered");
            var team = _mapper.Map<Team>(teamDto);
            await _context.Update(id, team);
        }
        public async Task<TeamDTO> GetSingle(int id)
        {
            await _queueService.Post("Loading single team was triggered");
            var team = await _context.GetSingleItem(id);
            return _mapper.Map<TeamDTO>(team);
        }
        public async Task<IEnumerable<TeamDTO>> Get()
        {
            await _queueService.Post("Loading all teams was triggered");
            var teams = await _context.GetItems();
            return _mapper.Map<IEnumerable<TeamDTO>>(teams);
        }
    }
}
