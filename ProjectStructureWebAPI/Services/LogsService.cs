﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureWebAPI.Hubs;
using ProjectStructureWebAPI.DTOs;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Services
{
    public class LogsService
    {
        private readonly IHubContext<LogsHub> _hub;
        List<MessageModelDTO> logs = new List<MessageModelDTO>();

        public LogsService(IHubContext<LogsHub> hub)
        {
            _hub = hub;
        }

        public async Task SendRequestToRecieveLogs()
        {
            await _hub.Clients.All.SendAsync("Logs");
            await Task.Delay(500);
            await _hub.Clients.All.SendAsync("LogsReceived", logs);
        }

        public async Task CreateLogs(IEnumerable<MessageModelDTO> args)
        {
            await Task.Run(() =>
            {
                logs = (List<MessageModelDTO>)args;
            });
        }
    }
}
