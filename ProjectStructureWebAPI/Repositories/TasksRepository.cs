﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Repositories
{
    public class TasksRepository: IRepository<Models.Task>
    {
        private readonly DataContext _context;

        public TasksRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Models.Task>> GetItems()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<Models.Task> GetSingleItem(int id)
        {
            return await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async System.Threading.Tasks.Task Create(Models.Task entity)
        {
            await _context.Tasks.AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, Models.Task entity)
        {
            var taskToUpdate = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);

            taskToUpdate.Name = entity.Name;
            taskToUpdate.Description = entity.Description;
            taskToUpdate.FinishedAt = entity.FinishedAt;
            taskToUpdate.PerformerId = entity.PerformerId;
            taskToUpdate.ProjectId = entity.ProjectId;
            taskToUpdate.State = entity.State;

            _context.Tasks.Update(taskToUpdate);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var entity = await _context.Tasks.FirstOrDefaultAsync(p => p.Id == id);
            _context.Tasks.Remove(entity);
            await _context.SaveChangesAsync();
        }



    }
}
