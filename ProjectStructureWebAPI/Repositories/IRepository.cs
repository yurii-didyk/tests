﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Repositories
{
    public interface IRepository<T> where T: class
    {
        Task<IEnumerable<T>> GetItems();
        Task<T> GetSingleItem(int id);
        Task Create(T entity);
        Task Update(int id, T entity);
        Task Delete(int id);
    }
}
