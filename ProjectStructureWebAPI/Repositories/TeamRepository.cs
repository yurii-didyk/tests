﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Repositories
{
    public class TeamRepository: IRepository<Team>
    {
        private readonly DataContext _context;

        public TeamRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Team>> GetItems()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<Team> GetSingleItem(int id)
        {
            return await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async System.Threading.Tasks.Task Create(Team entity)
        {
            await _context.Teams.AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, Team entity)
        {
            var teamToUpdate = await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);

            teamToUpdate.Name = entity.Name;

            _context.Teams.Update(teamToUpdate);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var entity = await _context.Teams.FirstOrDefaultAsync(p => p.Id == id);
            _context.Teams.Remove(entity);
            await _context.SaveChangesAsync();
        }



    }
}
