﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Repositories
{
    public class UserRepository: IRepository<User>
    {
        private readonly DataContext _context;

        public UserRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<User>> GetItems()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<User> GetSingleItem(int id)
        {
            return await _context.Users.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async System.Threading.Tasks.Task Create(User entity)
        {
            await _context.Users.AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, User entity)
        {
            var userToUpdate = await _context.Users.FirstOrDefaultAsync(p => p.Id == id);

            userToUpdate.FirstName =entity.FirstName;
            userToUpdate.LastName = entity.LastName;
            userToUpdate.TeamId = entity.TeamId;
            userToUpdate.Email = entity.Email;
            userToUpdate.Birthday = entity.Birthday;

            _context.Users.Update(userToUpdate);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var entity = await _context.Users.FirstOrDefaultAsync(p => p.Id == id);
            _context.Users.Remove(entity);
            await _context.SaveChangesAsync();
        }



    }
}
