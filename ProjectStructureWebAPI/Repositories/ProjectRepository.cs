﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProjectStructureWebAPI.Models;
using ProjectStructureWebAPI.Context;
using Microsoft.EntityFrameworkCore;

namespace ProjectStructureWebAPI.Repositories
{
    public class ProjectRepository: IRepository<Project>
    {
        private readonly DataContext _context;

        public ProjectRepository(DataContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Project>> GetItems()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task<Project> GetSingleItem(int id)
        {
            return await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async System.Threading.Tasks.Task Create(Project entity)
        {
            await _context.Projects.AddAsync(entity);
            await _context.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task Update(int id, Project entity)
        {
            var projectToUpdate = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);

            projectToUpdate.Name = entity.Name;
            projectToUpdate.Description = entity.Description;
            projectToUpdate.Deadline = entity.Deadline;
            projectToUpdate.TeamId = entity.TeamId;

            _context.Projects.Update(projectToUpdate);
            await _context.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int id)
        {
            var entity = await _context.Projects.FirstOrDefaultAsync(p => p.Id == id);
            _context.Projects.Remove(entity);
            await _context.SaveChangesAsync();
        }



    }
}
