﻿using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Interfaces
{
    public interface IMessageQueue: IDisposable
    {
        void BindQueue(string exchangeName, string routingKey, string queueName);
        void DeclareExchange(string exchangeName, string exchangeType);
        IModel Channel { get; }
        
    }
}
