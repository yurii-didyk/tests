﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructureWebAPI.Interfaces
{
    public interface IMessageProducer
    {
        void Send(string message, string type = null);
        void SendTyped(Type type, string message);

    }
}
