﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RabbitMQ.Client;


namespace ProjectStructureWebAPI.MessageSettingsModels
{
    public class MessageConsumerSettings
    {
        public bool SequentialFetch { get; set; } = true;
        public bool AutoAcknowledge { get; set; } = false;
        public IModel Channel { get; set; }
        public string QueueName { get; set; }
    }
}
