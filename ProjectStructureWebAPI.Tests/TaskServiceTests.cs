﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using Moq;
using NUnit.Framework;
using ProjectStructureWebAPI.Repositories;
using ProjectStructureWebAPI.Models;
using Microsoft.AspNetCore;
using ProjectStructureWebAPI.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Tests
{
    [TestFixture]
    class TaskServiceTests
    {
        private Mock<IRepository<Task>> taskRepository = new Mock<IRepository<Task>>();
        private DependencyResolverHelper _serviceProvider;
        private IMapper mapper;
        private TaskService _service;

        public TaskServiceTests()
        {
            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
            mapper = _serviceProvider.GetService<IMapper>();
            _service = new TaskService(taskRepository.Object,
                mapper, _serviceProvider.GetService<QueueService>());
        }

        [SetUp]
        public void TestSetup()
        {
            var tasks = new List<Task>
            {
                new Models.Task
                {
                    Id = 1,
                    Description = "Description for task 1",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task1",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 2,
                    Description = "Description for task 2",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task2",
                    PerformerId = 2,
                    ProjectId = 2,
                    State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 3,
                    Description = "Description for task 3",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task3",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Finished
                }
            };
            taskRepository.Setup(t => t.GetItems()).Returns(System.
               Threading.Tasks.Task.FromResult((IEnumerable<Task>)tasks));
            taskRepository.Setup(t => t.GetSingleItem(It.IsAny<int>())).Returns(System.
               Threading.Tasks.Task.FromResult(tasks.ElementAt(1)));
            taskRepository.Setup(t => t.Update(It.IsAny<int>(), It.IsAny<Task>())).Verifiable();
        }

        [Test]
        public async System.Threading.Tasks.Task ChangeTaskStateToFinished()
        {
            var task = await _service.GetSingle(2);
            task.State = TaskState.Finished;
            await _service.Update(2, task);

            taskRepository.Verify(t => t.Update(It.IsAny<int>(), It.IsAny<Task>()), Times.Once);
        }
    }
}
