﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using Moq;
using NUnit.Framework;
using ProjectStructureWebAPI.Repositories;
using ProjectStructureWebAPI.Models;
using Microsoft.AspNetCore;
using ProjectStructureWebAPI.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Tests
{
    [TestFixture]
    class TeamServiceTests
    {
        private Mock<IRepository<Team>> teamRepository = new Mock<IRepository<Team>>();
        private DependencyResolverHelper _serviceProvider;
        private IMapper mapper;
        private TeamService _service;

        public TeamServiceTests()
        {
            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
            mapper = _serviceProvider.GetService<IMapper>();
            _service = new TeamService(teamRepository.Object,
                mapper, _serviceProvider.GetService<QueueService>());
        }

        [SetUp]
        public void TestSetup()
        {
            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    CreatedAt = DateTime.Now,
                    Name = "team1"
                },
                new Team
                {
                    Id = 2,
                    CreatedAt = DateTime.Now.AddYears(-1),
                    Name = "teamnumber2"
                }
            };
            teamRepository.Setup(t => t.GetItems()).Returns(System.
              Threading.Tasks.Task.FromResult((IEnumerable<Team>)teams));
            teamRepository.Setup(t => t.Update(It.IsAny<int>(), It.IsAny<Team>())).Callback((int id, Team team) =>
            {
                var item = teams.FirstOrDefault(i => i.Id == id);
                item.Users = team.Users;
            });
        }

        [Test]
        public async System.Threading.Tasks.Task AddUserToTeam()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Birthday = DateTime.Now.AddYears(-15),
                    Email = "gmail3@gmail.com",
                    FirstName = "Sam",
                    LastName = "Washington",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                },
                new User
                {
                    Id = 2,
                    Birthday = DateTime.Now.AddYears(-13),
                    Email = "gmail4@gmail.com",
                    FirstName = "Aundey",
                    LastName = "Melly",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                }
            };

            var teams = await _service.Get();
            var team = mapper.Map<Team>(teams.FirstOrDefault());
            team.Users = users;
            await teamRepository.Object.Update(1, team);

            teamRepository.Verify(t => t.Update(It.IsAny<int>(), It.IsAny<Team>()), Times.Once);
            var item = (await teamRepository.Object.GetItems()).FirstOrDefault();
            Assert.That(item.Users.Count == 2);
        }

    }

}
