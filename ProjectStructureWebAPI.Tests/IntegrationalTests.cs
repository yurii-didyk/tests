﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.Context;
using Moq;
using ProjectStructureWebAPI.Models;
using AutoFixture;
using ProjectStructureWebAPI.Helpers;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Repositories;
using ProjectStructureWebAPI.DTOs;
using Microsoft.AspNetCore.Hosting;
using ProjectStructureWebAPI.QueueServices;
using ProjectStructureWebAPI.MessageSettingsModels;
using AutoMapper;
using System.Linq;
using Newtonsoft.Json;
using ProjectStructureWebAPI.Interfaces;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureWebAPI.Hubs;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore.SqlServer;

namespace ProjectStructureWebAPI.Tests
{
    [TestFixture]
    public class IntegrationalTests
    {
        private TestServer _testServer;
        private HttpClient _httpClient;

        [SetUp]
        public void TestSetup()
        {
            var webhost = new WebHostBuilder().UseStartup<Startup>().ConfigureServices(services =>
            {
                services.AddDbContext<DataContext>(options => options.UseInMemoryDatabase("DB"));
            });
            _testServer = new TestServer(webhost);
            _httpClient = _testServer.CreateClient();
        }

        [Test]
        public async System.Threading.Tasks.Task CreateProject()
        {
            var project = new ProjectDTO
            {
                Id = 1,
                Name = "test project",
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddDays(5),
                AuthorId = 1,
                TeamId = 1,
                Description = "decription for project one"
            };

            var response = await _httpClient.PostAsync("/api/projects", new StringContent(JsonConvert.SerializeObject(project), Encoding.UTF8, "application/json"));

            Assert.That(response.IsSuccessStatusCode == true);
        }

        [Test]
        public async System.Threading.Tasks.Task CreateTeam()
        {
            var project = new TeamDTO
            {
                Id = 1,
                Name = "first team",
                CreatedAt = DateTime.Now,
            };

            var response = await _httpClient.PostAsync("/api/teams", new StringContent(JsonConvert.SerializeObject(project), Encoding.UTF8, "application/json"));

            Assert.That(response.IsSuccessStatusCode == true);
        }

        [Test]
        public async System.Threading.Tasks.Task DeleteUser()
        {
            var user = new UserDTO
            {
                Id = 1,
                Birthday = DateTime.Now,
                Email = "gmail@mail.ru",
                FirstName = "Andrii",
                LastName = "Petrov",
                RegisteredAt = DateTime.Now,
                TeamId = 1
            };
            var r = await _httpClient.PostAsJsonAsync("/api/users", user);

            var response = await _httpClient.DeleteAsync("/api/users/1");

            Assert.That(response.IsSuccessStatusCode == true);
        }

        [Test]
        public async System.Threading.Tasks.Task DeleteTask()
        {
            var task = new TaskDTO
            {
                Id = 1,
                CreatedAt = DateTime.Now,
                Description = " description",
                Name = "first task",
                PerformerId = 1,
                FinishedAt = DateTime.Now.AddDays(6),
                ProjectId = 1,
                State = TaskState.Canceled
            };
            var r = await _httpClient.PostAsJsonAsync("/api/tasks", task);

            var response = await _httpClient.DeleteAsync("/api/tasks/1");

            Assert.That(response.IsSuccessStatusCode == true);
        }

        [Test]
        public async System.Threading.Tasks.Task FinishedTasksInThisYear()
        {
            var task = new TaskDTO
            {
                Id = 1,
                CreatedAt = DateTime.Now,
                Description = " description",
                Name = "first task",
                PerformerId = 1,
                FinishedAt = DateTime.Now.AddDays(6),
                ProjectId = 1,
                State = TaskState.Finished
            };
            var r = await _httpClient.PostAsJsonAsync("/api/tasks", task);

            var response = await _httpClient.GetAsync("/api/queries/3/1") ;

            Assert.That(response.IsSuccessStatusCode == true && 
                JsonConvert.DeserializeObject<List<Tuple<int, string>>>(await response.Content.ReadAsStringAsync()).FirstOrDefault().Item1 == 1);
        }
    }
}
