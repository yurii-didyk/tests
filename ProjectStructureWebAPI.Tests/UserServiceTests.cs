﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit;
using Moq;
using NUnit.Framework;
using ProjectStructureWebAPI.Repositories;
using ProjectStructureWebAPI.Models;
using Microsoft.AspNetCore;
using ProjectStructureWebAPI.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.DTOs;

namespace ProjectStructureWebAPI.Tests
{
    [TestFixture]
    class UserServiceTests
    {
        private Mock<IRepository<User>> userRepository = new Mock<IRepository<User>>();
        private DependencyResolverHelper _serviceProvider;
        private IMapper mapper;
        private UserService _service;

        public UserServiceTests()
        {
            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
            mapper = _serviceProvider.GetService<IMapper>();
            _service = new UserService(userRepository.Object,
                mapper, _serviceProvider.GetService<QueueService>());
        }
        [SetUp]
        public void TestSetup()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Birthday = DateTime.Now.AddYears(-15),
                    Email = "gmail@gmail.com",
                    FirstName = "Josh",
                    LastName = "Singleton",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                },
                new User
                {
                    Id = 2,
                    Birthday = DateTime.Now.AddYears(-13),
                    Email = "gmail2@gmail.com",
                    FirstName = "Alisa",
                    LastName = "AbstractFactory",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                }
            };
            userRepository.Setup(t => t.GetItems()).Returns(System.
               Threading.Tasks.Task.FromResult((IEnumerable<User>)users));
            userRepository.Setup(t => t.Create(It.IsAny<User>())).Verifiable();
        }

        [Test]
        public async System.Threading.Tasks.Task AddUser()
        {
            var user = new User
            {
                Id = 3,
                Birthday = DateTime.Now,
                Email = "email@ukr.net",
                FirstName = "Dmitro",
                LastName = "Vasylenko",
                RegisteredAt = DateTime.Now.AddHours(-30),
                TeamId = 1
            };
            var userDto = mapper.Map<UserDTO>(user);
            await _service.Add(userDto);

            userRepository.Verify(i => i.Create(It.IsAny<User>()), Times.Once);
        }




    }
}
