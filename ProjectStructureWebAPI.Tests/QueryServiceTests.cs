﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;
using ProjectStructureWebAPI.Services;
using ProjectStructureWebAPI.Context;
using Moq;
using ProjectStructureWebAPI.Models;
using AutoFixture;
using ProjectStructureWebAPI.Helpers;
using Microsoft.AspNetCore;
using Microsoft.EntityFrameworkCore;
using ProjectStructureWebAPI.Repositories;
using ProjectStructureWebAPI.DTOs;
using Microsoft.AspNetCore.Hosting;
using ProjectStructureWebAPI.QueueServices;
using ProjectStructureWebAPI.MessageSettingsModels;
using AutoMapper;
using System.Linq;
using ProjectStructureWebAPI.Interfaces;
using Microsoft.AspNetCore.SignalR;
using ProjectStructureWebAPI.Hubs;

namespace ProjectStructureWebAPI.Tests
{
    [TestFixture]
    class QueryServiceTests
    {
        private DependencyResolverHelper _serviceProvider;
        private Mock<IRepository<User>> userRepository = new Mock<IRepository<User>>();
        private Mock<IRepository<Project>> projectRepository = new Mock<IRepository<Project>>();
        private Mock<IRepository<Team>> teamRepository = new Mock<IRepository<Team>>();
        private Mock<IRepository<Task>> taskRepository = new Mock<IRepository<Task>>();
        private IMapper mapper;
        private QueryService _service;

        public QueryServiceTests()
        {
            var webHost = WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .Build();
            _serviceProvider = new DependencyResolverHelper(webHost);
            mapper = _serviceProvider.GetService<IMapper>();
            _service = new QueryService(taskRepository.Object, teamRepository.Object,
                userRepository.Object, projectRepository.Object,
                mapper, _serviceProvider.GetService<QueueService>());

        }

        [SetUp]
        public void TestSetup()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = 1,
                    Birthday = DateTime.Now.AddYears(-15),
                    Email = "gmail@gmail.com",
                    FirstName = "Josh",
                    LastName = "Singleton",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                },
                new User
                {
                    Id = 2,
                    Birthday = DateTime.Now.AddYears(-13),
                    Email = "gmail2@gmail.com",
                    FirstName = "Alisa",
                    LastName = "AbstractFactory",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                }
            };

            var teams = new List<Team>
            {
                new Team
                {
                    Id = 1,
                    CreatedAt = DateTime.Now,
                    Name = "team1"
                }
            };

            var projects = new List<Project>
            {
                new Project
                {
                    Id = 1,
                    Name = "Project1",
                    AuthorId = 2,
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now,
                    Description = "Description for project 1",
                    TeamId = 1
                },
                new Project
                {
                    Id = 2,
                    Name = "Project2",
                    AuthorId = 1,
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now,
                    Description = "Description for project 2",
                    TeamId = 1
                }
            };

            var tasks = new List<Task>
            {
                new Models.Task
                {
                    Id = 1,
                    Description = "Description for task 1",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now.AddDays(1),
                    Name = "Task1",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 2,
                    Description = "Description for task 2",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task2",
                    PerformerId = 2,
                    ProjectId = 2,
                    State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 3,
                    Description = "Description for task 3",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task3",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Finished
                }

            };
           
            taskRepository.Setup(t => t.GetItems()).Returns(System.
                Threading.Tasks.Task.FromResult((IEnumerable<Task>)tasks));
            userRepository.Setup(t => t.GetItems()).Returns(System.
                Threading.Tasks.Task.FromResult((IEnumerable<User>)users));
            teamRepository.Setup(t => t.GetItems()).Returns(System.
                Threading.Tasks.Task.FromResult((IEnumerable<Team>)teams));
            projectRepository.Setup(t => t.GetItems()).Returns(System.
                Threading.Tasks.Task.FromResult((IEnumerable<Project>)projects));
        }
        [Test]
        [TestCase(1)]
        public async System.Threading.Tasks.Task TasksCountForProject_When_Giving_Id_Then_Returns_Correct_Object(int userId)
        {
            // arrange
            var project = new Project
            {
                Id = 2,
                Name = "Project2",
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now,
                Description = "Description for project 2",
                TeamId = 1
            };

            var expected_result = new TaskCountForProjectDTO
            {
                Project = mapper.Map<ProjectDTO>(project),
                TasksCount = 1
            };
            // act 
            var result = (await _service.TasksCountForProject(userId)).FirstOrDefault();

            // assert
            Assert.That(expected_result.TasksCount == result.TasksCount
                && expected_result.Project.Id == result.Project.Id);

        }

        [Test]
        [TestCase(1)]
        public async System.Threading.Tasks.Task TasksForPerformer_When_Giving_Id_Returns_Correct_Object(int userId)
        {
            
            // arrange
            var expected_result = new List<Task>
            {
                new Models.Task
                {
                    Id = 1,
                    Description = "Description for task 1",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task1",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Created
                },
                new Models.Task
                {
                    Id = 3,
                    Description = "Description for task 3",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task3",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Finished
                }
            };
            // act
            var result = await _service.TasksForPerformer(userId);

            // assert
            Assert.That(expected_result.Count == result.Count &&
                result.ElementAt(1).Id == expected_result.ElementAt(1).Id);
        }
        [Test]
        [TestCase(1)]
        public async System.Threading.Tasks.Task FinishedTasksInThisYear_When_Giving_Id_Returns_Correct_Object(int userId)
        {
            // arrange
            var expected_result = new List<Tuple<int, string>>
            {
                new Tuple<int, string>(3, "Task3")   
            };

            // act
            var result = await _service.FinishedTasksInThisYear(userId);

            // assert
            Assert.That(result.Count == expected_result.Count);
        }

        [Test]
        public async System.Threading.Tasks.Task TeamsOlder12_When_Invoke_Retrurns_Correct_Object()
        {
            // arrange
            var expected_result = new List<Tuple<int, string, List<User>>>
            {
                new Tuple<int, string, List<User>>(1, "team1", new List<User>
                {
                    new User
                    {
                         Id = 1,
                         Birthday = DateTime.Now.AddYears(-15),
                         Email = "gmail@gmail.com",
                         FirstName = "Josh",
                         LastName = "Singleton",
                         RegisteredAt = DateTime.Now,
                         TeamId = 1
                    },
                    new User
                    {
                         Id = 2,
                         Birthday = DateTime.Now.AddYears(-13),
                         Email = "gmail2@gmail.com",
                         FirstName = "Alisa",
                         LastName = "AbstractFactory",
                         RegisteredAt = DateTime.Now,
                         TeamId = 1
                    }
                })
            };

            // act
            var result = await _service.TeamsOlder12();

            // assert
            Assert.That(result.Count == expected_result.Count &&
                result.FirstOrDefault().Item1 == expected_result.FirstOrDefault().Item1 &&
                 result.FirstOrDefault().Item2 == expected_result.FirstOrDefault().Item2 &&
                  result.FirstOrDefault().Item3.Count == expected_result.FirstOrDefault().Item3.Count);
        }

        [Test]
        public async System.Threading.Tasks.Task GetSortedUsers_Returns_Correct_Sequence()
        {
            
            // arrange
            var expected_result = new List<Tuple<User, List<Task>>>
            {
                new Tuple<User, List<Task>>(
                     new User
                    {
                        Id = 2,
                        Birthday = DateTime.Now.AddYears(-13),
                        Email = "gmail2@gmail.com",
                        FirstName = "Alisa",
                        LastName = "AbstractFactory",
                        RegisteredAt = DateTime.Now,
                        TeamId = 1
                    },
                     new List<Task>
                     {
                         new Models.Task
                        {
                            Id = 2,
                            Description = "Description for task 2",
                            CreatedAt = DateTime.Now,
                            FinishedAt = DateTime.Now,
                            Name = "Task2",
                            PerformerId = 2,
                            ProjectId = 2,
                            State = TaskState.Created
                        }
                     }),
                new Tuple<User, List<Task>>(
                    new User
                    {
                        Id = 1,
                        Birthday = DateTime.Now.AddYears(-15),
                        Email = "gmail@gmail.com",
                        FirstName = "Josh",
                        LastName = "Singleton",
                        RegisteredAt = DateTime.Now,
                        TeamId = 1
                    },
                    new List<Task>
                     {
                          new Models.Task
                        {
                            Id = 1,
                            Description = "Description for task 1",
                            CreatedAt = DateTime.Now,
                            FinishedAt = DateTime.Now,
                            Name = "Task1",
                            PerformerId = 1,
                            ProjectId = 1,
                            State = TaskState.Created
                        },
                        new Models.Task
                        {
                            Id = 3,
                            Description = "Description for task 3",
                            CreatedAt = DateTime.Now,
                            FinishedAt = DateTime.Now,
                            Name = "Task3",
                            PerformerId = 1,
                            ProjectId = 1,
                            State = TaskState.Finished
                        }
                     })
                    
            };
            // act
            var result = await _service.GetSortedUsers();

            // assert
            Assert.That(result.Count == expected_result.Count &&
                result.FirstOrDefault().Item2.Count == expected_result.FirstOrDefault().Item2.Count &&
                result.FirstOrDefault().Item1.Id == expected_result.FirstOrDefault().Item1.Id);
        }

        [Test]
        [TestCase(1)]
        public async System.Threading.Tasks.Task GetModel1_Returns_Correct_Model(int userId)
        {
           
            // arrange
            var expected_result = new HelperModel1
            {
                CanceledAndUnfinishedTasksCount =  1,
                LastProject =  new Project
                {
                    Id = 2,
                    Name = "Project2",
                    AuthorId = 1,
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now,
                    Description = "Description for project 2",
                    TeamId = 1
                },
                LongestTask = new Models.Task
                {
                    Id = 1,
                    Description = "Description for task 1",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task1",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Created
                },
                TasksCount = 1,
                User = new User
                {
                    Id = 1,
                    Birthday = DateTime.Now.AddYears(-15),
                    Email = "gmail@gmail.com",
                    FirstName = "Josh",
                    LastName = "Singleton",
                    RegisteredAt = DateTime.Now,
                    TeamId = 1
                }
            };
            // act
            var result = await _service.GetModel1(userId);
            // assert
            Assert.That(result.User.Id == expected_result.User.Id &&
                result.TasksCount == expected_result.TasksCount &&
                result.LastProject.Id == expected_result.LastProject.Id &&
                result.LongestTask.Id == expected_result.LongestTask.Id &&
                result.CanceledAndUnfinishedTasksCount == expected_result.CanceledAndUnfinishedTasksCount);
        }

        [Test]
        [TestCase(1)]
        public async System.Threading.Tasks.Task GetModel2_Returns_Correct_Model(int projectId)
        {
            // arrange
            var expected_result = new HelperModel2
            {
                LongestTaskByDescription = new Models.Task
                {
                    Id = 3,
                    Description = "Description for task 3",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task3",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Finished
                },
                Project = new Project
                {
                    Id = 1,
                    Name = "Project1",
                    AuthorId = 2,
                    CreatedAt = DateTime.Now,
                    Deadline = DateTime.Now,
                    Description = "Description for project 1",
                    TeamId = 1
                },
                UsersCount = 2,
                LongestTaskByName = new Models.Task
                {
                    Id = 3,
                    Description = "Description for task 3",
                    CreatedAt = DateTime.Now,
                    FinishedAt = DateTime.Now,
                    Name = "Task3",
                    PerformerId = 1,
                    ProjectId = 1,
                    State = TaskState.Finished
                }
            };
            //act
            var result = await _service.GetModel2(projectId);
            //assert
            Assert.That(result.Project.Id == expected_result.Project.Id &&
                result.UsersCount == expected_result.UsersCount &&
                result.LongestTaskByName.Id == expected_result.LongestTaskByName.Id &&
                result.LongestTaskByDescription.Id == expected_result.LongestTaskByDescription.Id);
            
        }
    }

}

        


