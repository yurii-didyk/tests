﻿using System;
using System.Collections.Generic;
using System.Text;
using Worker.Models;
using Newtonsoft.Json;
using System.IO;
using Microsoft.Extensions.Configuration;
using ProjectStructureWebAPI;
using System.Linq;

namespace Worker
{
    class Logger: ILogger
    {
        AppConfiguration config = new AppConfiguration();

        public void WriteLog(MessageModel model)
        {
            using (StreamWriter sw = new StreamWriter(config.Log, append:true))
            {
                var json = JsonConvert.SerializeObject(model);
                sw.WriteLine(json);
            }
        }
        public IEnumerable<MessageModel> ReadLogAsCollection()
        {
            using (StreamReader sr = new StreamReader(config.Log))
            {
                var json = sr.ReadToEnd();
                string[] strs = json.Split('\n');
                List<MessageModel> list = new List<MessageModel>();
                foreach (var item in strs)
                {
                    list.Add(JsonConvert.DeserializeObject<MessageModel>(item));
                }
                return list.Where(i => i != null).OrderByDescending(t => t.CreatedAt);
            }
        }

    }
}
