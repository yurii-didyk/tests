﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Worker.Models
{
    class MessageModel
    {
        public string Message { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
